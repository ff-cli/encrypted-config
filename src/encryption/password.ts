import zxcvbn from 'zxcvbn';
import {generate} from 'generate-password';

import consts from './consts';


export const validatePassword = (password: string): boolean | Error => {
    const res = zxcvbn(password);
    if (res.score < 4) {
        let msg = [];
        if (res.feedback.warning) {
            msg.push(res.feedback.warning);
        }
        if (res.feedback.suggestions && res.feedback.suggestions.length > 0) {
            msg = msg.concat(res.feedback.suggestions);
        }
        if (msg.length == 0) {
            msg = [consts.VALIDATE_PASSWORD_FAILED_MSG_DEFAULT];
        }
        const e = new Error(msg.join("\n"));
        e.name = consts.VALIDATE_PASSWORD_FAILED_CODE_DEFAULT;

        return e;
    }

    return true;
};

export const generatePassword = (length?: number): string => {
    if (!length) {
        length = 50;
    }

    let password = generate({
        length: length,
        numbers: true,
        symbols: true,
        uppercase: true,
        strict: true
    });

    password = password.replace('\n', '');
    password = password.replace('"', '');
    password = password.replace("'", '');

    return password.trim();
};
