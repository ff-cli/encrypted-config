import crypto, {HexBase64BinaryEncoding, Utf8AsciiBinaryEncoding} from 'crypto';

import consts from "./consts";


export const encrypt = async (data: string, password: string, salt?: string, algorithm?: string, encoding?: string): Promise<string | Error> => {
    if (!salt) {
        salt = consts.SALT_DEFAULT;
    }
    if (!encoding) {
        encoding = consts.ENCODING_DEFAULT;
    }
    if (!algorithm) {
        algorithm = consts.ALGORITHM_DEFAULT;
    }
    // @ts-ignore
    return new Promise((resolve, reject) => {
        crypto.scrypt(password, salt, 32, (err, key) => {
            if (err) {
                reject(err);
                return err;
            }
            // generate a random initialization vector
            crypto.randomFill(new Uint8Array(16), (err, iv) => {
                if (err) {
                    reject(err);
                    return err;
                }
                const cipher = crypto.createCipheriv(algorithm, key, iv);

                // let encrypted = cipher.update(data, <Utf8AsciiBinaryEncoding>encoding, <HexBase64BinaryEncoding>'hex');
                let encrypted = cipher.update(data, 'ascii', 'hex');
                encrypted += cipher.final('hex');

                resolve(encrypted);
            });
        });
    });
};

export const decrypt = async (data: string, password: string, salt?: string, algorithm?: string, encoding?: string): Promise<string | Error> => {
    if (!salt) {
        salt = consts.SALT_DEFAULT;
    }
    if (!encoding) {
        encoding = consts.ENCODING_DEFAULT;
    }
    if (!algorithm) {
        algorithm = consts.ALGORITHM_DEFAULT;
    }

    // @ts-ignore
    return new Promise((resolve, reject) => {
        crypto.scrypt(password, salt, 32, (err, key) => {
            if (err) {
                reject(err);
                return err;
            }
            // generate a random initialization vector
            crypto.randomFill(new Uint8Array(16), (err, iv) => {
                if (err) {
                    reject(err);
                    return err;
                }
                const decipher = crypto.createDecipheriv(algorithm, key, iv);

                // let decrypted = decipher.update(data, <HexBase64BinaryEncoding>'hex', <Utf8AsciiBinaryEncoding>encoding);
                let decrypted = decipher.update(data, 'hex', 'ascii');
                decrypted += decipher.final('ascii');

                resolve(decrypted);
            });
        });
    });
};
