import fs from "fs";

import consts from "./consts";
import {decrypt, encrypt} from "./data";

export const encryptFile = async (from: string, to: string, password: string, encoding?: string): Promise<Error | boolean> => {
    if (!fs.existsSync(from)) {
        return new Error(consts.FILE_NOT_FOUND)
    }

    try {
        const buf = fs.readFileSync(from, {encoding: encoding});
        const encrypted = await encrypt(buf.toString(), consts.ALGORITHM_DEFAULT, password, encoding);
        fs.writeFileSync(to, encrypted, {encoding: encoding});
    } catch (e) {
        return e;
    }

    return true;
};

export const decryptFile = async (from: string, to: string, password: string, encoding?: string): Promise<Error | boolean> => {
    if (!fs.existsSync(from)) {
        return new Error(consts.FILE_NOT_FOUND)
    }

    try {
        const buf = fs.readFileSync(from, {encoding: encoding});
        const decrypted = await decrypt(buf.toString(), consts.ALGORITHM_DEFAULT, password, encoding);
        console.log(decrypted);
        fs.writeFileSync(to, decrypted, {encoding: encoding});
    } catch (e) {
        return e;
    }

    return true;
};

export const encryptDataToFile = async (data: any, to: string, password: string, encoding?: string): Promise<Error | boolean> => {
    let dataStr = '';

    try {
        if (typeof data !== 'string') {
            dataStr = JSON.stringify(data);
        } else {
            dataStr = data;
        }

        const encrypted = await encrypt(dataStr, consts.ALGORITHM_DEFAULT, password, encoding);
        fs.writeFileSync(to, encrypted, {encoding: encoding});
    } catch (e) {
        return e;
    }

    return true;
};

export const decryptDataFromFile = async (from: string, password: string, encoding?: string): Promise<Error | string> => {
    if (!fs.existsSync(from)) {
        return new Error(consts.FILE_NOT_FOUND)
    }

    try {
        const buf = fs.readFileSync(from, {encoding: encoding});
        const decrypted = await decrypt(buf.toString(), consts.ALGORITHM_DEFAULT, password, encoding);

        return decrypted;
    } catch (e) {
        return e;
    }
};
