export const consts = {
    VALIDATE_PASSWORD_FAILED_MSG_DEFAULT: 'The chosen password was not strong enough. You must choose a stronger password, ' +
        'in order to keep your data safe.',
    VALIDATE_PASSWORD_FAILED_CODE_DEFAULT: 'WEAK',
    FILE_NOT_FOUND: 'File not found',

    ALGORITHM_DEFAULT: 'aes-256-cbc',
    // SALT_DEFAULT: '0123456789abcdefghijklmnopqrstuvwxyz',
    SALT_DEFAULT: '0123456789abcdef',
    ENCODING_DEFAULT: 'utf8',
};

export default consts;